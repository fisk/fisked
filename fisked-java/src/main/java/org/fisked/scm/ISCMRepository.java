package org.fisked.scm;

public interface ISCMRepository {
	String getBranchName();
	String getSCMName();
}
