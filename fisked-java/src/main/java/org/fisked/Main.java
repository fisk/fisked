package org.fisked;

public class Main {
	public static void main(String[] args) {
		Application application = Application.getApplication();
		application.start(args);
	}
}
