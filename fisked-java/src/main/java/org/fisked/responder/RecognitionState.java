package org.fisked.responder;

public enum RecognitionState {
	Recognized,
	MaybeRecognized,
	NotRecognized
}
