package org.fisked.responder;

public interface IRecognitionAction {
	void onRecognize();
}
