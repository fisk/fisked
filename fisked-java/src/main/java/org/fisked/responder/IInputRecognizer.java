package org.fisked.responder;

public interface IInputRecognizer {
	RecognitionState recognizesInput(Event nextEvent);
}
