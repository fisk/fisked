package org.fisked.buffer.drawing;

public interface IDrawable {
	void draw();
}
