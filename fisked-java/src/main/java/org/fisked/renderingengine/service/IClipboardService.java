package org.fisked.renderingengine.service;

public interface IClipboardService {
	String getClipboard();
	void setClipboard(String value);
}
