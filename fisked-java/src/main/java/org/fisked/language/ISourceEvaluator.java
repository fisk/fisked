package org.fisked.language;

public interface ISourceEvaluator {
	String evaluate(String val);
}
